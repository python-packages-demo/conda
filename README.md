# [conda](https://pypi.org/project/conda/)

Package manager used by Anaconda installations. https://conda.io

## Other similar demo using conda
* [python-packages-demo/nbconvert](https://gitlab.com/python-packages-demo/nbconvert)